import { CourseCard } from '../CourseCard/CourseCard';

function CoursesList({ data, authors }) {
	return (
		<ul>
			{data.map((el) => (
				<CourseCard
					key={el.id}
					title={el.title}
					description={el.description}
					authorsIds={el.authors}
					duration={el.duration}
					creationDate={el.creationDate}
					allAuthors={authors}
				/>
			))}
		</ul>
	);
}

export { CoursesList };
