import { useEffect, useState } from 'react';
import { Button } from '../../../../common/Button/Button';
import { Input } from '../../../../common/Input/Input';
import styles from './SearchBar.module.css';

function SearchBar({ filter }) {
	const [searchPhrase, setSearchPhrase] = useState('');

	function handleSearch(e) {
		e.preventDefault();
		filter(searchPhrase);
	}

	useEffect(() => {
		if (searchPhrase === '') {
			filter('');
		}
	}, [filter, searchPhrase]);

	return (
		<form className={styles.searchBar} onSubmit={handleSearch}>
			<Input
				className={styles.input}
				placeholdetText='Enter course name...'
				value={searchPhrase}
				onChange={(e) => setSearchPhrase(e.target.value)}
			/>
			<Button>Search</Button>
		</form>
	);
}

export { SearchBar };
