import { Button } from '../../../../common/Button/Button';
import styles from './CourseCard.module.css';
import { pipeDuration } from '../../../../helpers/pipeDuration';
import { findAuthorById } from '../../../../helpers/findAuthorById';
import { beautifyDate } from '../../../../helpers/beautifyDate';

function CourseCard({
	title,
	description,
	authorsIds,
	duration,
	creationDate,
	allAuthors,
}) {
	return (
		<div className={styles.courseCard}>
			<div className={styles.left}>
				<h2 className={styles.title}>{title}</h2>
				<p className={styles.description}>{description}</p>
			</div>

			<div className={styles.right}>
				<div className={styles.authors}>
					<span className={styles.span}>Authors: </span>
					{authorsIds
						.map((authorId) => findAuthorById(authorId, allAuthors))
						.join(', ')}
				</div>
				<div className={styles.duration}>
					<span className={styles.span}>Duration: </span>
					{pipeDuration(duration)} hours
				</div>
				<div className={styles.creationDate}>
					<span className={styles.span}>Created: </span>
					{beautifyDate(creationDate)}
				</div>
				<Button className={styles.button}>Show course</Button>
			</div>
		</div>
	);
}

export { CourseCard };
