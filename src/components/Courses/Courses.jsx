import { SearchBar } from '../../components/Courses/components/SearchBar/SearchBar';
import { Button } from '../../common/Button/Button';
import { CoursesList } from './components/CoursesList/CoursesList';
import styles from './Courses.module.css';
import { useCallback, useState } from 'react';

function Courses({ closeCourses, courses, authors }) {
	const [filteredList, setFilteredList] = useState(courses);

	const filter = useCallback(
		(searchParam) =>
			setFilteredList(
				courses.filter(
					(el) =>
						el.title.toLowerCase().includes(searchParam.toLowerCase()) ||
						el.id.toLowerCase().includes(searchParam.toLowerCase())
				)
			),
		[courses]
	);

	return (
		<div className={styles.courses}>
			<div className={styles.top}>
				<SearchBar filter={filter} />
				<Button onClick={closeCourses}>Add new course</Button>
			</div>

			{filteredList.length ? (
				<CoursesList data={filteredList} authors={authors} />
			) : (
				<div>No such courses found</div>
			)}
		</div>
	);
}

export { Courses };
