import { Input } from '../../../../common/Input/Input';
import { Button } from '../../../../common/Button/Button';
import { v4 as uuidv4 } from 'uuid';

import styles from './AddAuthor.module.css';

function AddAuthor({ authors, setAuthors }) {
	function addNewAuthor(e) {
		setAuthors([
			...authors,
			{ id: uuidv4(), name: e.target.previousSibling.children[1].value },
		]);

		e.target.previousSibling.children[1].value = '';
	}

	return (
		<div>
			<h3 className={styles.title}>Add Author</h3>
			<Input
				labelText='Author name'
				placeholdetText='Enter author name'
				className={styles.input}
			/>
			<Button className={styles.button} onClick={addNewAuthor}>
				Create author
			</Button>
		</div>
	);
}

export { AddAuthor };
