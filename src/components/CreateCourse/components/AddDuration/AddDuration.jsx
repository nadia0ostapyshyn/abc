import { Input } from '../../../../common/Input/Input';
import { pipeDuration } from '../../../../helpers/pipeDuration';

import styles from './AddDuration.module.css';

function AddDuration({ newCourse, setNewCourse }) {
	return (
		<div>
			<h3 className={styles.title}>Duration</h3>
			<Input
				labelText='Duration'
				placeholdetText='Enter duration in minutes'
				className={styles.input}
				onChange={(e) =>
					setNewCourse({ ...newCourse, duration: Number(e.target.value) })
				}
				required
			/>
			<div className={styles.duration}>
				Duration{' '}
				<span className={styles.durationSpan}>
					{pipeDuration(newCourse.duration)}
				</span>{' '}
				hours
			</div>
		</div>
	);
}

export { AddDuration };
