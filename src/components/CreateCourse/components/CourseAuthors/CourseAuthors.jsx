import { Button } from '../../../../common/Button/Button';
import { findAuthorById } from '../../../../helpers/findAuthorById';
import styles from './CourseAuthors.module.css';

function CourseAuthors({ authors, newCourse, setNewCourse }) {
	function deleteAuthorFromCourse(id) {
		setNewCourse({
			...newCourse,
			authors: newCourse.authors.filter((authorId) => id !== authorId),
		});
	}

	return (
		<div>
			<h3 className={styles.title}>Course authors</h3>
			<ul className={styles.courseAuthorsList}>
				{newCourse.authors.length ? (
					newCourse.authors.map((authorId) => (
						<li className={styles.authorItem} key={authorId}>
							<div className={styles.authorName}>
								{findAuthorById(authorId, authors)}
							</div>
							<Button onClick={() => deleteAuthorFromCourse(authorId)}>
								Delete author
							</Button>
						</li>
					))
				) : (
					<p className={styles.message}>Authors list is empty</p>
				)}
			</ul>
		</div>
	);
}

export { CourseAuthors };
