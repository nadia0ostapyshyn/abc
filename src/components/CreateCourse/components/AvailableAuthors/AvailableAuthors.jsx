import { Button } from '../../../../common/Button/Button';
import styles from './AvailableAuthors.module.css';

function AvailableAuthors({ authors, newCourse, setNewCourse }) {
	function addAuthorToCourse(id) {
		setNewCourse({ ...newCourse, authors: [...newCourse.authors, id] });
	}

	return (
		<div>
			<h3 className={styles.title}>Authors</h3>
			<ul className={styles.availableAuthorsList}>
				{authors
					.filter((author) => !newCourse.authors.includes(author.id))
					.map((author) => (
						<li className={styles.authorItem} key={author.id}>
							<div className={styles.authorName}>{author.name}</div>
							<Button onClick={() => addAuthorToCourse(author.id)}>
								Add author
							</Button>
						</li>
					))}
			</ul>
		</div>
	);
}

export { AvailableAuthors };
