import { Button } from '../../common/Button/Button';
import { Input } from '../../common/Input/Input';
import { Textarea } from '../../common/Textarea/Textarea';
import { AddAuthor } from '../CreateCourse/components/AddAuthor/AddAuthor';
import { AvailableAuthors } from '../CreateCourse/components/AvailableAuthors/AvailableAuthors';
import { AddDuration } from '../CreateCourse/components/AddDuration/AddDuration';
import { CourseAuthors } from '../CreateCourse/components/CourseAuthors/CourseAuthors';
import styles from './CreateCourse.module.css';
import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { dateGenerator } from '../../helpers/dateGenerator';

function CreateCourse({
	courses,
	setCourses,
	authors,
	setAuthors,
	addNewCourse,
}) {
	const [newCourse, setNewCourse] = useState({
		id: '',
		title: '',
		description: '',
		creationDate: '',
		duration: 0,
		authors: [],
	});

	const VALID_FIELDS = {
		description: (value) => {
			return value.length >= 2;
		},
		duration: (value) => {
			return value > 0 && typeof value === 'number';
		},
	};

	function isCourseValid(course) {
		let isError = true;
		const failedFields = [];

		Object.keys(course).forEach((field) => {
			if (
				!!VALID_FIELDS[field] &&
				VALID_FIELDS[field](course[field]) === false
			) {
				failedFields.push(field);
				isError = false;
			}
		});

		return { isError: isError, failedFields: failedFields };
	}

	function showErrors(failedFields) {
		alert(`Please correct invalid fields: ${failedFields.join(', ')}
		Description should be at least 2 characters;
		Duration should be a number that is more than 0`);
	}

	function handleSumbit(e) {
		e.preventDefault();
		const course = {
			...newCourse,
			id: uuidv4(),
			creationDate: dateGenerator(new Date().toLocaleDateString()),
		};

		if (isCourseValid(course).isError) {
			setCourses([...courses, course]);
			addNewCourse();
		} else {
			showErrors(isCourseValid(course).failedFields);
		}
	}

	return (
		<form className={styles.createCourse} onSubmit={handleSumbit}>
			<div className={styles.top}>
				<Input
					labelText='Title'
					placeholdetText='Enter title'
					className={styles.titleInput}
					value={newCourse.title}
					onChange={(e) =>
						setNewCourse({ ...newCourse, title: e.target.value })
					}
					required
				/>
				<Textarea
					labelText='Description'
					placeholdetText='Enter description'
					className={styles.descriptionTextarea}
					value={newCourse.description}
					onChange={(e) =>
						setNewCourse({ ...newCourse, description: e.target.value })
					}
					required
				/>
				<Button className={styles.addCourseButton}>Create Course</Button>
			</div>

			<div className={styles.bottom}>
				<AddAuthor
					className={styles.addAuthor}
					authors={authors}
					setAuthors={setAuthors}
				/>
				<AvailableAuthors
					className={styles.availableAuthors}
					authors={authors}
					newCourse={newCourse}
					setNewCourse={setNewCourse}
				/>
				<AddDuration
					className={styles.addDuration}
					newCourse={newCourse}
					setNewCourse={setNewCourse}
				/>
				<CourseAuthors
					className={styles.courseAuthors}
					authors={authors}
					newCourse={newCourse}
					setNewCourse={setNewCourse}
				/>
			</div>
		</form>
	);
}

export { CreateCourse };
