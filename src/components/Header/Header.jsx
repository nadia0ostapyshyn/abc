import { Button } from '../../common/Button/Button';
import { Logo } from './components/Logo/Logo';
import styles from './Header.module.css';

function Header() {
	return (
		<header className={styles.header}>
			<Logo />
			<div className={styles.headerInfo}>
				<p className={styles.username}>Dave</p>
				<Button className={styles.button}>Logout</Button>
			</div>
		</header>
	);
}

export { Header };
