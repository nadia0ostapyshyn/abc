import { useState } from 'react';
import { Courses } from './components/Courses/Courses';
import { Header } from './components/Header/Header';
import { CreateCourse } from './components/CreateCourse/CreateCourse';
import { mockedCoursesList, mockedAuthorsList } from './constants';

function App() {
	const [showCoursesList, setShowCoursesList] = useState(true);
	const [courses, setCourses] = useState(mockedCoursesList);
	const [authors, setAuthors] = useState(mockedAuthorsList);

	function hideCoursesList() {
		setShowCoursesList(false);
	}

	function addNewCourse() {
		setShowCoursesList(true);
	}

	return (
		<div>
			<Header />
			{showCoursesList ? (
				<Courses
					closeCourses={hideCoursesList}
					courses={courses}
					authors={authors}
				/>
			) : (
				<CreateCourse
					courses={courses}
					setCourses={setCourses}
					authors={authors}
					setAuthors={setAuthors}
					addNewCourse={addNewCourse}
				/>
			)}
		</div>
	);
}
export default App;
