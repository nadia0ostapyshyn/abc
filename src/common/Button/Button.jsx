import styles from './Button.module.css';

function Button({ children, className, ...props }) {
	return (
		<button {...props} className={`${styles.button} ${className}`}>
			{children}
		</button>
	);
}

export { Button };
