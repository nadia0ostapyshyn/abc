import styles from './Input.module.css';

function Input({ labelText, placeholdetText, className, onChange, ...props }) {
	return (
		<label className={styles.label} htmlFor='input'>
			{labelText && <span className={styles.span}>{labelText}</span>}
			<input
				className={`${styles.input} ${className}`}
				type='text'
				id='input'
				placeholder={placeholdetText}
				onChange={onChange}
				{...props}
			/>
		</label>
	);
}

export { Input };
