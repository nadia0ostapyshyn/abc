import styles from './Textarea.module.css';

function Textarea({
	labelText,
	placeholdetText,
	className,
	onChange,
	...props
}) {
	return (
		<label htmlFor='textarea' className={`${styles.label} ${className}`}>
			{labelText && <span className={styles.span}>{labelText}</span>}
			<textarea
				className={styles.textarea}
				id='textarea'
				placeholder={placeholdetText}
				onChange={onChange}
				{...props}
			></textarea>
		</label>
	);
}

export { Textarea };
