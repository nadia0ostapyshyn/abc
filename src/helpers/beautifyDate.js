function beautifyDate(dateParam) {
	return dateParam.split('/').join('.');
}

export { beautifyDate };
