function pipeDuration(mins) {
	const hours = Math.floor(mins / 60);
	const minutes = mins % 60;
	let hoursToDisplay;
	let minutesToDisplay;

	if (hours < 10) {
		hoursToDisplay = `0${hours}`;
	} else {
		hoursToDisplay = hours;
	}

	if (minutes < 10) {
		minutesToDisplay = `0${minutes}`;
	} else {
		minutesToDisplay = minutes;
	}

	return `${hoursToDisplay}:${minutesToDisplay}`;
}

export { pipeDuration };
