function dateGenerator(dateParam) {
	const date = new Date(dateParam);
	const day = date.getDate();
	const month = date.getMonth() + 1;
	const year = date.getFullYear();

	return [day, month, year].join('.');
}

export { dateGenerator };
