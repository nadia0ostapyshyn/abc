function findAuthorById(id, authors) {
	const foundAuthor = authors.find((author) => author.id === id);
	return foundAuthor.name;
}

export { findAuthorById };
